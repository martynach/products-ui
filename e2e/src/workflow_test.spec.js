jest.setTimeout(30000);

const webdriver = require('selenium-webdriver');

const connectionString = process.env.CI ? 'http://172.17.0.4:4200' : 'http://localhost:4200';
let driver;

const productPattern = {
  productName: 'test',
  unitPrice: '999',
  quantityPerUnit: '1 sztuka',
  unitsInStock: '100',
  unitsOnOrder: '345',
  category: {
    categoryName: 'test category',
    description: 'To jest testowy produkt, który powinien być usunięty',
  },
  supplier: {
    companyName: 'test company name',
    contactName: 'test contact name',
    contactTitle: 'test contact title',
    address: 'MountView Road',
    city: 'Kraków',
    region: 'Debniki',
    postalCode: '30-340',
    country: 'Polska',
    phone: '685-583-573',
    fax: '685-583-573',
    homePage: "www.costamcostam.pl"
  }
};
const expectedProductDetailHeader = 'Szczegóły produktu: ' + productPattern.productName;

const expectedProductDetailBody = 'Nazwa:\n' +
  'test\n' +
  '\n' +
  'Kategoria:\n' +
  productPattern.category.categoryName.toUpperCase() + '\n' +
  'To jest testowy produkt, który powinien być usunięty\n' +
  '\n' +
  'Dostawca:\n' +
  productPattern.supplier.companyName.toUpperCase() + '\n' +
  productPattern.supplier.address + ', ' + productPattern.supplier.city + ', ' + productPattern.supplier.country + '\n' +
  productPattern.supplier.phone + '\n' +
  '\n' +
  'W magazynie:\n' +
  'tak (' + productPattern.unitsInStock + ' jednostek)\n' +
  '\n' +
  'Cena jednostkowa:\n' +
  'PLN' + Number.parseFloat(productPattern.unitPrice).toFixed(2) + '\n' +
  '\n' +
  'Ilość sztuk w jednostce:\n' +
  productPattern.quantityPerUnit + '\n' +
  '\n' +
  'Minimalna ilość jednostek w zamówieniu:\n' +
  productPattern.unitsOnOrder;

const editProductNameSuffix = 'tralalala';


describe('General e2e tests of Product Management App', async () => {

  async function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  beforeAll(async () => {
    driver = await new webdriver.Builder().forBrowser('chrome').build();
    sleep(5000);
    await driver.get(connectionString);
  });


  afterAll(async () => {
    await driver.quit();
  });


  it('Testing welcome page', async () => {
    await sleep(3000);
    await expect(Promise.resolve(driver.findElement(webdriver.By.linkText('Strona główna'), 1000))).resolves.toBeDefined();
    await expect(Promise.resolve(driver.findElement(webdriver.By.linkText('Lista produktów'), 1000))).resolves.toBeDefined();
    await expect(Promise.resolve(driver.findElement(webdriver.By.linkText('Dodaj produkt'), 1000))).resolves.toBeDefined();
    await expect(Promise.resolve(driver.findElement(webdriver.By.className('img-responsive'), 1000))).resolves.toBeDefined();
  });

  describe('Testing creating new product', async () => {

    it('Should go to newproduct page', async () => {
      await sleep(3000);
      await driver.findElement(webdriver.By.linkText('Dodaj produkt'), 1000).click();
    });

    it('Should fill the form for new product', async () => {
      await sleep(3000);
      await driver.findElement(webdriver.By.name('inputProductName'), 1000).sendKeys(productPattern.productName);
      await driver.findElement(webdriver.By.name('inputUnitPrice'), 1000).sendKeys(productPattern.unitPrice);
      await driver.findElement(webdriver.By.name('inputQuantityPerUnit'), 1000).sendKeys(productPattern.quantityPerUnit);
      await driver.findElement(webdriver.By.name('inputUnitsInStock'), 1000).sendKeys(productPattern.unitsInStock);
      await driver.findElement(webdriver.By.name('inputUnitsOnOrder'), 1000).sendKeys(productPattern.unitsOnOrder);
      await driver.findElement(webdriver.By.name('categoryRadio'), 1000).click();
      await driver.findElement(webdriver.By.name('inputCategoryName'), 1000).sendKeys(productPattern.category.categoryName);
      await driver.findElement(webdriver.By.name('inputCategoryDescription'), 1000).sendKeys(productPattern.category.description);
      await driver.findElement(webdriver.By.name('supplierRadio'), 1000).click();
      await driver.findElement(webdriver.By.name('inputSupplierCompanyName'), 1000).sendKeys(productPattern.supplier.companyName);
      await driver.findElement(webdriver.By.name('inputSupplierContactName'), 1000).sendKeys(productPattern.supplier.contactName);
      await driver.findElement(webdriver.By.name('inputSupplierAddress'), 1000).sendKeys(productPattern.supplier.address);
      await driver.findElement(webdriver.By.name('inputSupplierCity'), 1000).sendKeys(productPattern.supplier.city);
      await driver.findElement(webdriver.By.name('inputSupplierRegion'), 1000).sendKeys(productPattern.supplier.region);
      await driver.findElement(webdriver.By.name('inputSupplierPostalCode'), 1000).sendKeys(productPattern.supplier.postalCode);
      await driver.findElement(webdriver.By.name('inputSupplierCountry'), 1000).sendKeys(productPattern.supplier.country);
      await driver.findElement(webdriver.By.name('inputSupplierPhone'), 1000).sendKeys(productPattern.supplier.phone);
      await driver.findElement(webdriver.By.name('inputSupplierFax'), 1000).sendKeys(productPattern.supplier.fax);
      await driver.findElement(webdriver.By.name('inputSupplierHomePage'), 1000).sendKeys(productPattern.supplier.homePage);
    });

    it('Should appear alert informing that new product was created', async () => {
      await driver.findElement(webdriver.By.id('submit')).click();
      await sleep(3000);
      const expectedText = 'Utworzono nowy produkt: ' + productPattern.productName;
      await expect(Promise.resolve(driver.switchTo().alert().getText())).resolves.toEqual(expectedText);
      await driver.switchTo().alert().accept();
    });

    it('Product filters should be visible on product list page', async () => {
      await sleep(3000);
      await driver.findElement(webdriver.By.name('categoryName'), 1000).sendKeys(productPattern.category.categoryName);
      await driver.findElement(webdriver.By.name('companyName'), 1000).sendKeys(productPattern.supplier.companyName);
      await driver.findElement(webdriver.By.name('priceFrom'), 1000).sendKeys(productPattern.unitPrice - 1);
      await driver.findElement(webdriver.By.name('priceTo'), 1000).sendKeys(productPattern.unitPrice - (-1));
      await driver.findElement(webdriver.By.id('submit')).click();
    });

    it('Newly created product should appear on product list and link to product detail page', async () => {
      await sleep(3000);
      await driver.findElement(webdriver.By.linkText('test'), 1000).click();
    });
  });

  describe('Testing editing existing product', async () => {

    it('Product detail page should have all elements', async () => {

      await sleep(1000);

      const existingHeader = await driver.findElement(webdriver.By.className('card-header'), 1000).getText()
      expect(existingHeader).toEqual(expectedProductDetailHeader);

      const existingBody = await driver.findElement(webdriver.By.className('card-body'), 1000).getText()
      expect(existingBody).toEqual(expectedProductDetailBody);

      await driver.findElement(webdriver.By.id('editProduct'), 1000).click();
    });

    it('Edited correctly edit product', async () => {

      await sleep(1000);

      await driver.findElement(webdriver.By.name('inputProductName'), 1000).sendKeys(editProductNameSuffix);
      await sleep(1000);

      await driver.findElement(webdriver.By.id('submit')).click();
      await sleep(1000);

      const textAfterEditingProduct = await driver.switchTo().alert().getText();
      const expectedTextAfterEditingProduct = 'Poprawnie zmieniono produkt: ' + productPattern.productName;
      expect(textAfterEditingProduct).toEqual(expectedTextAfterEditingProduct);

      await driver.switchTo().alert().accept();
    });

    it('Product filters should be visible on product list page (2)', async () => {
      await driver.findElement(webdriver.By.name('categoryName'), 1000).sendKeys(productPattern.category.categoryName);
      await driver.findElement(webdriver.By.name('companyName'), 1000).sendKeys(productPattern.supplier.companyName);
      await driver.findElement(webdriver.By.name('priceFrom'), 1000).sendKeys(productPattern.unitPrice - 1);
      await driver.findElement(webdriver.By.name('priceTo'), 1000).sendKeys(productPattern.unitPrice - (-1));
      await driver.findElement(webdriver.By.id('submit')).click();
    });

    it('Old name of product should not exist', async () => {
      await sleep(1000);
      await expect(Promise.resolve(driver.findElement(webdriver.By.linkText(productPattern.productName), 1000).click())).rejects.toThrowError();
    });

    it('New name of product should be on product list and link to product detail', async () => {
      await driver.findElement(webdriver.By.linkText(productPattern.productName + editProductNameSuffix), 1000).click();
    });

  });

  describe('Testing deleting existing product', async () => {

    it('Delete button should be clicked', async () => {
      await sleep(1000);
      await driver.findElement(webdriver.By.id('deleteProduct'), 1000).click();
    });

    it('Product filters should be visible on product list page (3)', async () => {
      await sleep(1000);
      await driver.findElement(webdriver.By.name('categoryName'), 1000).sendKeys(productPattern.category.categoryName);
      await driver.findElement(webdriver.By.name('companyName'), 1000).sendKeys(productPattern.supplier.companyName);
      await driver.findElement(webdriver.By.name('priceFrom'), 1000).sendKeys(productPattern.unitPrice - 1);
      await driver.findElement(webdriver.By.name('priceTo'), 1000).sendKeys(productPattern.unitPrice - (-1));
      await driver.findElement(webdriver.By.id('submit')).click();
    });

    it('Deleted product should not exist on product list', async () => {
      await sleep(1000);
      await expect(Promise.resolve(driver.findElement(webdriver.By.linkText(productPattern.productName), 1000).click())).rejects.toThrowError();
      await expect(Promise.resolve(driver.findElement(webdriver.By.linkText(productPattern.productName + editProductNameSuffix), 1000).click())).rejects.toThrowError();
    });
  });

});

