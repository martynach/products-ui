import { Component, OnInit } from '@angular/core';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  pageTitle = 'WELCOME TO PRODUCTS MANAGEMENT SYSTEM';

  constructor() { }

  ngOnInit() {
    this.pageTitle += environment.environment;
  }

}
