export interface ISupplierQueryParams {
  fields?: string;
  companyName?: string;
  sortDescendingByName?: boolean;
  sortAscendingByName?: boolean;
}
