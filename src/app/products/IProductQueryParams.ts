export interface IProductQueryParams {
  fields?: string;
  limit: string;
  offset?: string;
  categoryName?: string;
  supplierName?: string;
  priceFrom?: string;
  priceTo?: string;
}
