export interface ISupplier {
    _id?: string;
    companyName?: string;
    contactName?: string;
    contactTitle?: string;
    address?: string;
    city?: string;
    region?: string;
    postalCode?: string;
    country?: string;
    phone?: string;
    fax?: string;
    homePage?: string;
}
