import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductDetailGuard implements CanActivate {

  constructor(private _router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    const id: string = next.url[1].path;
    // url has two path segments:
    // next.url[0].path: products
    // next.url[1].path: id
    if (!id || !id.match('[a-f,0-9]{24}')) {
      alert('Invalid product id: ' + id);
      this._router.navigate(['/products']);
      return false;
    }

    return true;
  }
}
