import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductService} from '../product.service';
import {IProduct} from '../IProduct';
import {PassingProductDataService} from '../passing-product-data.service';

@Component({
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  pageTitle = 'Szczegóły produktu';
  errorMessage: string;
  product: IProduct;

  constructor(private _route: ActivatedRoute,
              private _router: Router,
              private _productService: ProductService,
              private _passingDataService: PassingProductDataService) {
  }

  ngOnInit() {
    const param = this._route.snapshot.paramMap.get('id');
    if (param) {
      this.getProduct(param);
    }
  }

  getProduct(id: string) {
    this._productService.getProduct(id).subscribe(
      product => this.product = product,
      error => this.errorMessage = <any>error);
  }

  onBack() {
    this._router.navigate(['/products']);
  }

  onEdit() {
    this._passingDataService.product = this.product;
     this._router.navigate(['/newproduct']);
  }

  onDelete() {
    this._productService.removeProduct(this.product._id).subscribe(
      () => {
        this._router.navigate(['/products']);
      },
      (error) => {
        this.errorMessage = <any>error;
      }
    );
    console.log('onDelete()');
  }
}

