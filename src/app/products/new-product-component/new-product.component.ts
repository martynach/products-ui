import {Component, Input, OnInit} from '@angular/core';
import {IProduct} from '../IProduct';
import {PassingProductDataService} from '../passing-product-data.service';
import {ICategory} from '../ICategory';
import {ISupplier} from '../ISupplier';
import {ProductService} from '../product.service';
import {HttpErrorResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {ISupplierQueryParams} from '../ISupplierQueryParams';
import {ICategoryQueryParams} from '../ICategoryQueryParams';

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.css']
})
export class NewProductComponent implements OnInit {

  pageTitle: string;
  product: IProduct;
  isEditMode: boolean;

  newCategory = 'newCategory';
  existingCategory = 'existingCategory';
  categoryChoice = this.existingCategory;

  existingCategories: ICategory[] = [];

  newSupplier = 'newSupplier';
  existingSupplier = 'existingSupplier';
  supplierChoice = this.existingSupplier;

  existingSuppliers: ISupplier[] = [];

  constructor(private _passingDataService: PassingProductDataService, private _productService: ProductService, private _router: Router, ) {
  }

  ngOnInit() {
    this.pageTitle = this._passingDataService.pageTitle ? this._passingDataService.pageTitle : 'Dodawanie/edycja produktu';
    this.product = this._passingDataService.product ? this._passingDataService.product : {category: {}, supplier: {}};
    this.isEditMode = this._passingDataService.product ? true : false;

    this.getExistingCategories();
    this.getExistingSuppliers();
  }

  onSaveClick() {
    console.log('onSave event: product to save: ');
    console.log(this.product);

    if (this.isEditMode) {
      this.editProduct();
    } else {
      this.addNewProduct();
    }

  }

  addNewProduct() {
    this._productService.addProduct(this.product).subscribe((product: IProduct) => {
      alert('Utworzono nowy produkt: ' + product.productName);
      this._router.navigate(['/products']);
    }, (error: HttpErrorResponse) => {
      alert('Podczas próby zapisu wystąpił bład. Upewnij się, że folmularz jest poprawnie wypełniony i spróbuj ponownie.');
    });
  }

  editProduct() {
    this._productService.editProduct(this.product._id, this.product).subscribe((product: IProduct) => {
      alert('Poprawnie zmieniono produkt: ' + product.productName);
      this._passingDataService.product = undefined;
      this._router.navigate(['/products']);
    }, (error: HttpErrorResponse) => {
      alert('Podczas próby edycji wystąpił bład. Upewnij się, że folmularz jest poprawnie wypełniony i spróbuj ponownie.');
    });
  }

  onCancelClick() {
    console.log('onCancel');
    if (confirm('Czy na pewno chcesz opuścić stronę? Wszystkie zmiany zostaną utracone.')) {
      this._router.navigate(['/products']);
    }
  }

  getExistingCategories() {
    const categoryQueryParams: ICategoryQueryParams = {
      sortDescendingByName: true,
    };
    this._productService.getCategories(categoryQueryParams).subscribe(categories => {
      this.existingCategories = categories;
    }, error => {
      console.log(error);
    });
  }

  getExistingSuppliers() {
    const supplierQueryParams: ISupplierQueryParams = {
      sortDescendingByName: true,
    };
    this._productService.getSuppliers(supplierQueryParams).subscribe(suppliers => {
      this.existingSuppliers = suppliers;
    }, error => {
      console.log(error);
    });
  }

  compareById(obj1, obj2) {
    return obj1._id === obj2._id;
  }

}
