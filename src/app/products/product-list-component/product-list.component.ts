import {Component, OnInit} from '@angular/core';
import {IProduct} from '../IProduct';
import {ProductService} from '../product.service';
import {IProductQueryParams} from '../IProductQueryParams';

@Component({
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  constructor(private _productService: ProductService) {
    // we do not want long logic in constructor
    // this.products = this._productService.getProducts(); calls to backend service
    // so it cannot be in cosntructor
    // this.filteredProducts = this.products; moved to ngOnInit
    // because constructor is executed before and list of products would be empty
  }

  categoryName: string;
  supplierName: string;
  priceFrom: number;
  priceTo: number;

  queryParams: IProductQueryParams = {
    fields: 'productName,category.categoryName,supplier/companyName,unitPrice,unitsInStock',
    limit: '5',
    offset: '0',
  };

  pageTitle = 'Lista produktów';
  products: IProduct[] = [];

  ngOnInit() {
    this.getProductList();
  }

  applyFilters() {
    if (this.categoryName) {
      this.queryParams.categoryName = this.categoryName;
    } else {
      delete this.queryParams.categoryName;
    }

    if (this.supplierName) {
      this.queryParams.supplierName = this.supplierName;
    } else {
      delete this.queryParams.supplierName;
    }

    if (this.priceFrom) {
      this.queryParams.priceFrom = this.priceFrom + '';
    } else {
      delete this.queryParams.priceFrom;
    }

    if (this.priceTo) {
      this.queryParams.priceTo = this.priceTo + '';
    } else {
      delete this.queryParams.priceTo;
    }

    this.getProductList();
  }

  getProductList() {
    this._productService.getProducts(this.queryParams).subscribe(products => {
      this.products = products;
    }, error => {
      console.log(error);
    });
  }

}
