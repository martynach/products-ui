import {Injectable} from '@angular/core';
import {IProduct} from './IProduct';


@Injectable({
  providedIn: 'root'
})
export class PassingProductDataService {


  private _pageTitle: string;
  private _product: IProduct;

  get product(): IProduct {
    return this._product;
  }

  set product(value: IProduct) {
    this._product = value;
  }
  get pageTitle(): string {
    return this._pageTitle;
  }

  set pageTitle(value: string) {
    this._pageTitle = value;
  }






}
