import {ICategory} from './ICategory';
import {ISupplier} from './ISupplier';

export interface IProduct {
  _id?: string;
  productName?: string;
  supplier?: ISupplier;
  category?: ICategory;
  quantityPerUnit?: string;
  unitPrice?: number;
  unitsInStock?: number;
  unitsOnOrder?: number;
  reorderLevel?: number;
  discontinued?: boolean;
}
