export interface ICategoryQueryParams {
  fields?: string;
  categoryName?: string;
  description?: string;
  sortDescendingByName?: boolean;
  sortAscendingByName?: boolean;
}
