import {Injectable} from '@angular/core';
import {IProduct} from './IProduct';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IProductQueryParams} from './IProductQueryParams';
import {ICategory} from './ICategory';
import {ICategoryQueryParams} from './ICategoryQueryParams';
import {ISupplierQueryParams} from './ISupplierQueryParams';
import {ISupplier} from './ISupplier';
import {environment} from './../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  url: string = environment.productserviceUrl;

  constructor(private _httpClient: HttpClient) {
    console.log('Using productservice url: ' + this.url);
  }

  getProducts(queryParams: IProductQueryParams): Observable <IProduct[]> {

    let httpParams: HttpParams = new HttpParams();

    for (const param in queryParams) {
      httpParams = httpParams.append(param, queryParams[param]);
    }

    return this._httpClient.get<IProduct[]>(this.url + 'products', {params: httpParams});
  }

  getCategories(queryParams: ICategoryQueryParams): Observable <ICategory[]> {

    let httpParams: HttpParams = new HttpParams();

    for (const param in queryParams) {
      httpParams = httpParams.append(param, queryParams[param]);
    }

    return this._httpClient.get<ICategory[]>(this.url + 'categories', {params: httpParams});
  }

  getSuppliers(queryParams: ISupplierQueryParams): Observable <ISupplier[]> {

    let httpParams: HttpParams = new HttpParams();

    for (const param in queryParams) {
      httpParams = httpParams.append(param, queryParams[param]);
    }

    return this._httpClient.get<ISupplier[]>(this.url + 'suppliers', {params: httpParams});
  }

  getProduct(id: string): Observable<IProduct> {
    return this._httpClient.get<IProduct>(this.url + 'products/' + id);
  }

  addProduct(product: IProduct): Observable<IProduct> {
    return this._httpClient.post<IProduct>(this.url + 'products', product );
  }

  editProduct(productId: string, product: IProduct): Observable<IProduct> {
    return this._httpClient.post<IProduct>(this.url + 'products/' + productId, product );
  }

  removeProduct(id: string): Observable<Object> {
    return this._httpClient.delete(this.url + 'products/' + id);
  }

}
